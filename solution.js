var fs = require('fs'); 
const fsP = require('fs').promises;
const { v4: uuidv4 } = require('uuid');
let uid="";

const login=(userName,password)=>{
    return new Promise((resolve, reject) => {
        setTimeout(()=>{
            userExist(userName,password).then(res=>{
                switch (res) {
                    case 0:
                        //founde user and pass
                        resolve(true);
                        break;
                    case 1:
                        // password user is mismatch
                        throw new Error('Incorrect username/password: 400,')
                        break;
                    case 2:
                        // no user 
                        throw new Error('user not found: 404,')
                        break;
                
                    default:
                        // any other case
                        throw new Error('Internal Server error: 500 ')
                        break;
                }
            })
            .catch(res=>{
                // throw new Error(res);
                reject(res)
            })
            
        },4000)
    })   
}

const userExist=(userName,password)=>{
    return new Promise((resolve, reject) => {
    fsP.readFile('./login.json',{encoding:'utf-8'})
    .then(res=>JSON.parse(res))
    .then(data=>{
      let user=  Object.keys(data).includes(userName) 
      let pass=Object.values(data).includes(password)
      if(user){
        if(pass){
            resolve(0);
        }else{
            resolve(1);
        }
      }else{
          resolve(2)
      }
    })
    .catch(res=>{
        reject('Internal Server error: 500')
    })
    })
    
}
login("jim@microsoft.com", "jimmarketplace").then(res=>{
    uid=uuidv4()
    setTimeout(()=>{
        uid=""
    },2000)
    if(uid ){
        getUserData("walker@discord.com").then(userdata=>{
            console.log(userdata);
        }).catch(res=>console.log(res))
        gettodolist("jim@microsoft.com").then(todolist=>{
            console.log(todolist);
        }).catch(res=>console.log(res))
    }else{
        throw new Error('UnAuthorized: 403')
    }
})
.catch(err=>console.log(err))

const getUserData=(userName)=>{
    return new Promise((resolve, reject) => {
        setTimeout(()=>{
            fsP.readFile('./profile.json')
            .then(res=>JSON.parse(res))
            .then(data=>{
                let details=data[userName]
                resolve(details)
            }).catch(err=>{
                reject('Internal Server Error: 500')
            })
        },3000)
    })
}
const gettodolist=(userName)=>{
    return new Promise((resolve, reject) => {
        setTimeout(()=>{
            fsP.readFile('./todo-list.json')
            .then(res=>JSON.parse(res))
            .then(data=>{
                let details=data[userName]
                // if(details)
                resolve(details)
                
            }).catch(err=>{
                reject('Internal Server Error: 500')
            })
        },2000)
    })
}



// fsP.readFile('./ase.json',{encoding:'utf-8'}).then(res=>JSON.parse(res)).then(res=>console.log(res))